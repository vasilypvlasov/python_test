#!/usr/bin/env python

from itertools import permutations

MONTHS_30 = (4, 6, 9, 11)
YEAR_FROM = 2000
YEAR_TO = 3000


class DateParser(object):
    year = 0
    month = 0
    day = 0

    def __init__(self, date_line):
        self.date_line = date_line
        self._parse_date_string()
        self._order_datas()

    def get_string(self):
        return '{}-{}-{}'.format(self.year, self.month, self.day)

    def _parse_date_string(self):
        self.normal_date = sorted(
            map(int, self.date_line.split('/')))

    def _order_datas(self):
        for date in permutations(self.normal_date):
            self.year = date[0] + YEAR_FROM < YEAR_TO and date[0] + YEAR_FROM or date[0]
            self.month = date[1]
            self.day = date[2]
            if not self._condition_check_date():
                print('is illegal')

    def _condition_check_day(self):
        if self.day > 31:
            return False
        if self.month in MONTHS_30 and self.day > 30:
            return False
        if self.month is 2 and self.day > (self._condition_is_leap_year() and 29 or 28):
            return False
        return True

    def _condition_check_month(self):
        return self.month <= 12

    def _condition_check_year(self):
        return self.year >= 0 and self.year < 1000 or self.year >= YEAR_FROM and self.year < YEAR_TO

    def _condition_is_leap_year(self):
        return 'is leap year' if ((self.year % 4 == 0 and self.year % 100 != 0) or self.year % 400 == 0) else 'isn`t leap year'

    def _condition_check_date(self):
        return self._condition_check_day() and self._condition_check_month() and self._condition_check_year()


def main():
    with open('dates.txt') as f:
        date_line = f.readline()
    parser = DateParser(date_line)

    print(parser._condition_is_leap_year())
    print(parser.get_string())


if __name__ == '__main__':
    main()
